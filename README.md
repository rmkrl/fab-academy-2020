# Fab Academy 2020

This project is here to make it easier to collaborate on course issues. You can think of it as the website of a course. Here you can find information about the local shcedule and activities regarding the Fab Academy 2020 course. 

## Schedule

Please follow [the official Fab Academy 2020 schedule](http://fabacademy.org/2020/schedule.html) for global lecture and recitation dates. Locally the weekly structure is as follows.

- Wednesday 16:00: global lecture
- Thursday 15:00: local lecture
- Monday 16:00: global recitation

## When can I Work at the Lab?

Generally the lab is open from 10:00 to 16:00 every day from Monday to Friday. The difference is only when you can (or can not) book the machines. You can book the machines via the [Aalto Takeout system](https://takeout.aalto.fi).

- Monday (machines have to be booked)
- Tuesday (no booking, come as you are)
- Wednesday (maintenance day, open for Fab Academy)
- Thursday, Friday (machines have to be booked)

## Participants

- Oskar Koli
- [Jasmine Xie](https://gitlab.com/bluet87)
- [Boeun Kim](https://gitlab.com/bunnykim)
- [Wan-Ting Hsieh](https://gitlab.com/cv47522)
- Niklas Ekholm
- Anssi Alhopuro
- Niklas Pöllönen
- Solomon Kiflom
- Ranjit Menon

## Documentation

Every participant should have a documentation project hosted on their GitLab account. We use GitLab to maintain consistency and to learn the wizardry of Git. The name of your repository (GitLab project) should be **fab-academy**. Your documentation should end up being a static HTML website. We will cover how to automatically publish it on every `git push` using GitLab CI/CD during the class. The recommended content structure of your documentation website should be as follows.

- About
- Final Project
- Assignments
  + Week 01: Project Management
  + Week 02: Computer-Aided Design
  + Week 03: Computer-Controlled Cutting
  + ...

## Weekly Assignments

Weekly assignments will be posted as per-issue basis. Every weekly assignment will be assigned an issue in this project. Please keep the discussion about weekly assignments in their respective issue pages. Please post general questions in the **General** issue.

